package com.mexchange.rest.util.co.profile;


import com.mexchange.core.utils.co.CoreUserDetailsCO;
import com.mexchange.rest.util.co.user.GetUserDetailsCO;
import org.hibernate.validator.constraints.NotBlank;

import javax.validation.constraints.NotNull;

public class GetProfileDetailsCO {

    @NotNull(message = "userco.username.null")
    @NotBlank(message = "userco.username.blank")
    private String username;
    private String parameters;

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getParameters() {
        return parameters;
    }

    public void setParameters(String parameters) {
        this.parameters = parameters;
    }

    public CoreUserDetailsCO getUserDetailsCO(){
        CoreUserDetailsCO coreUserDetailsCO=new CoreUserDetailsCO(this.username);
        coreUserDetailsCO.setParams(this.parameters);
        return coreUserDetailsCO;
    }
}
