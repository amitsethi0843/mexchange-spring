package com.mexchange.rest.util.functionalInterface;


import com.mexchange.rest.util.dto.ResponseDTO;

@FunctionalInterface
public interface ResponseInterface {
    ResponseDTO processResponse();

}
