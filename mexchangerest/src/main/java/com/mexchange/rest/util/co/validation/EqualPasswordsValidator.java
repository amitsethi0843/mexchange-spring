package com.mexchange.rest.util.co.validation;

import com.mexchange.rest.util.co.user.CreateUserCO;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

public class EqualPasswordsValidator implements ConstraintValidator<EqualPasswords,CreateUserCO>{
    @Override
    public void initialize(EqualPasswords constraint) {
    }

    @Override
    public boolean isValid(CreateUserCO userCO, ConstraintValidatorContext context) {
        if(userCO.getPassword()!=null && userCO.getRetypePassword()!=null) {
            return userCO.getPassword().equals(userCO.getRetypePassword());
        }
        return false;
    }
}
