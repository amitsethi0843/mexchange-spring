package com.mexchange.rest.util.vo;

import java.io.Serializable;


public class JwtAuthenticationResponse implements Serializable {
    private static final long serialVersionUID = 1250166508152483573L;
    private final String token;
    private final String username;
    private final String firstName;
    private final String lastName;

    public JwtAuthenticationResponse(String token, String username, String firstName, String lastName) {
        this.token = token;
        this.username = username;
        this.firstName = firstName;
        this.lastName = lastName;
    }

    public String getFirstName() {
        return this.firstName;
    }

    public String getLastName() {
        return this.lastName;
    }

    public String toString() {
        return "token=" + this.token + " username=" + this.username;
    }

    public String getToken() {
        return this.token;
    }

    public String getUsername() {
        return this.username;
    }
}
