package com.mexchange.rest.util;

import com.mexchange.core.service.user.CoreUserService;
import com.mexchange.core.utils.co.CoreCreateUserCO;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

@Service
public class Bootstrap implements InitializingBean {

    @Autowired
    private CoreUserService coreUserService;

    public void afterPropertiesSet() throws Exception {
        createDummyUser();
    }

    private void createDummyUser(){
        CoreCreateUserCO coreUserDetailsCO=new CoreCreateUserCO();
        coreUserDetailsCO.setFirstName("amit");
        coreUserDetailsCO.setLastName("sethi");
        coreUserDetailsCO.setUsername("amitsethi0843@gmail.com");
        coreUserDetailsCO.setPassword("1234");
        coreUserDetailsCO.setCotactNumber("9811242930");
        coreUserService.createUser(coreUserDetailsCO);
        System.out.println("created user amit sethi");
    }
}
