package com.mexchange.rest.util.co;

import com.mexchange.core.domain.address.Address;
import com.mexchange.core.domain.contact.Contact;
import com.mexchange.rest.util.co.adressContact.CreateAddressCO;
import com.mexchange.rest.util.co.adressContact.CreateContactCO;
import com.mexchange.rest.util.co.item.CreateItemCO;
import org.hibernate.validator.constraints.Email;
import org.hibernate.validator.constraints.NotBlank;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

public class CreateExchangeCO {

    @NotNull(message = "userco.password.null")
    @NotBlank(message = "userco.password.blank")
    @Size(min = 4,max = 100,message = "userco.password.size")
    private String itemName;

    @NotNull
    @NotBlank
    @Email
    private String username;

    @NotNull
    @NotBlank
    private Integer itemQuantity;

    private String itemPurchasedDate;

    @NotNull
    @NotBlank
    private String itemDescription;

    @NotNull
    @NotBlank
    private Boolean itemInWarranty;

    @NotNull
    @NotBlank
    private String adLocation;

    @NotNull
    @NotBlank
    private String adLatitude;

    @NotNull
    @NotBlank
    private String adLongitude;

    @NotNull
    @NotBlank
    private String adCountry;

    private String adState;

    private String adCity;

    private String adPinCode;

    private Address address;

    private Contact contact;

    @NotNull
    @NotBlank
    private String adContact;

    @NotNull
    @NotBlank
    private String adEmail;

    public Address getAddress() {
        return address;
    }

    public void setAddress(Address address) {
        this.address = address;
    }

    public Contact getContact() {
        return contact;
    }

    public void setContact(Contact contact) {
        this.contact = contact;
    }

    public String getItemName() {
        return itemName;
    }

    public void setItemName(String itemName) {
        this.itemName = itemName;
    }

    public Integer getItemQuantity() {
        return itemQuantity;
    }

    public void setItemQuantity(Integer itemQuantity) {
        this.itemQuantity = itemQuantity;
    }

    public String getItemPurchasedDate() {
        return itemPurchasedDate;
    }

    public void setItemPurchasedDate(String itemPurchasedDate) {
        this.itemPurchasedDate = itemPurchasedDate;
    }

    public String getItemDescription() {
        return itemDescription;
    }

    public void setItemDescription(String itemDescription) {
        this.itemDescription = itemDescription;
    }

    public Boolean getItemInWarranty() {
        return itemInWarranty;
    }

    public void setItemInWarranty(Boolean itemInWarranty) {
        this.itemInWarranty = itemInWarranty;
    }

    public String getAdLocation() {
        return adLocation;
    }

    public void setAdLocation(String adLocation) {
        this.adLocation = adLocation;
    }

    public String getAdLatitude() {
        return adLatitude;
    }

    public void setAdLatitude(String adLatitude) {
        this.adLatitude = adLatitude;
    }

    public String getAdLongitude() {
        return adLongitude;
    }

    public void setAdLongitude(String adLongitude) {
        this.adLongitude = adLongitude;
    }

    public String getAdCountry() {
        return adCountry;
    }

    public void setAdCountry(String adCountry) {
        this.adCountry = adCountry;
    }

    public String getAdState() {
        return adState;
    }

    public void setAdState(String adState) {
        this.adState = adState;
    }

    public String getAdCity() {
        return adCity;
    }

    public void setAdCity(String adCity) {
        this.adCity = adCity;
    }

    public String getAdPinCode() {
        return adPinCode;
    }

    public void setAdPinCode(String adPinCode) {
        this.adPinCode = adPinCode;
    }

    public String getAdContact() {
        return adContact;
    }

    public void setAdContact(String adContact) {
        this.adContact = adContact;
    }

    public String getAdEmail() {
        return adEmail;
    }

    public void setAdEmail(String adEmail) {
        this.adEmail = adEmail;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public CreateItemCO getCreateItemCO(){
        CreateItemCO createItemCO=new CreateItemCO();
        createItemCO.setName(this.itemName);
        return createItemCO;
    }

    public CreateContactCO getCreateContactCO(){
        CreateContactCO createContactCO=new CreateContactCO();
        createContactCO.setContact(this.getAdContact());
        return createContactCO;
    }

    public CreateAddressCO getCreateAddressCO(){
        CreateAddressCO createAddressCO=new CreateAddressCO();
        createAddressCO.setLatitude(this.getAdLatitude());
        createAddressCO.setLongitude(this.getAdLongitude());
        createAddressCO.setLocation(this.getAdLocation());
        createAddressCO.setCountry(this.getAdCountry());
        createAddressCO.setState(this.getAdState());
        createAddressCO.setPinCode(this.getAdPinCode());
        createAddressCO.setCity(this.getAdCity());
        return createAddressCO;
    }
}
