package com.mexchange.rest.util.co.user;

import com.mexchange.rest.util.co.validation.EqualPasswords;
import org.hibernate.validator.constraints.NotBlank;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import com.mexchange.core.utils.co.CoreCreateUserCO;

@EqualPasswords
public class CreateUserCO {

    @NotNull(message = "userco.username.null")
    @NotBlank(message = "userco.username.blank")
    private String username;

    @NotNull(message = "userco.firstName.null")
    @NotBlank(message = "userco.firstName.blank")
    private String firstName;

    @NotNull(message = "userco.lastName.null")
    @NotBlank(message = "userco.lastName.blank")
    private String lastName;

    @NotNull(message = "userco.password.null")
    @NotBlank(message = "userco.password.blank")
    @Size(min = 4,max = 100,message = "userco.password.size")
    private String password;

    @NotNull(message = "userco.retypePassword.null")
    @NotBlank(message = "userco.retypePassword.blank")
    @Size(min = 4,max = 100,message = "userco.retypePassword.size")
    private String retypePassword;

    public String getRetypePassword() {
        return retypePassword;
    }

    public void setRetypePassword(String retypePassword) {
        this.retypePassword = retypePassword;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public CoreCreateUserCO getCoreUserCO(){
        CoreCreateUserCO coreCreateUserCO=new CoreCreateUserCO();
        coreCreateUserCO.setUsername(this.username);
        coreCreateUserCO.setPassword(this.password);
        coreCreateUserCO.setFirstName(this.firstName);
        coreCreateUserCO.setLastName(this.lastName);
        return coreCreateUserCO;

    }
}
