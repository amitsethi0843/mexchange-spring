package com.mexchange.rest.util.vo;

import com.mexchange.core.domain.exchange.ExchangeInstance;

/**
 * Created by amitsethi on 3/18/18.
 */
public class CreateExchangeVO {

    private String exchangeUuid;

    public String getExchangeUuid() {
        return exchangeUuid;
    }

    public void setExchangeUuid(String exchangeUuid) {
        this.exchangeUuid = exchangeUuid;
    }

    public CreateExchangeVO(){}

    public CreateExchangeVO(ExchangeInstance exchangeInstance){
        this.setExchangeUuid(exchangeInstance.getUuid());
    }
}
