package com.mexchange.rest.util.co.adressContact;

import com.mexchange.core.utils.co.addressContact.CoreCreateContactCO;

/**
 * Created by amitsethi on 4/1/18.
 */
public class CreateContactCO {
    private String contact;

    public String getContact() {
        return contact;
    }

    public void setContact(String contact) {
        this.contact = contact;
    }

    public CoreCreateContactCO getCoreCreateContactCO(){
        CoreCreateContactCO coreCreateContactCO=new CoreCreateContactCO();
        coreCreateContactCO.setContact(this.getContact());
        return coreCreateContactCO;
    }
}
