package com.mexchange.rest.util.dto;

import java.io.Serializable;


public class ResponseDTO implements Serializable{

    private Object response;

    private String errorCode;

    private String message;

    private boolean status;

    public boolean isStatus() {
        return status;
    }

    public void setStatus(boolean status) {
        this.status = status;
    }

    public ResponseDTO(){}

    public String getErrorCode() {
        return errorCode;
    }

    public void setErrorCode(String errorCode) {
        this.errorCode = errorCode;
    }

    public ResponseDTO(Object response, String errorCode, String message){
        this.response=response;
        this.errorCode=errorCode;
        this.message=message;
    }

    public Object getResponse() {
        return response;
    }

    public void setResponse(Object response) {
        this.response = response;
    }


    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }


    public ResponseDTO(String errorCode, String message){
        this.errorCode=errorCode;
        this.message=message;
    }

   public void createSuccessResponseDTOInstance(Object response, String message,String errorCode){
       this.response=response;
       this.message=message;
       this.errorCode=errorCode!=null?errorCode:"200";
       this.status=true;
   }

   @Override
   public String toString(){
       return "message="+this.message+" errorCode="+this.errorCode+" status="+this.status;
   }



}
