package com.mexchange.rest.util.co.user;

import com.mexchange.core.utils.co.CoreRetrieveUserCO;
import org.hibernate.validator.constraints.NotBlank;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

public class LoginUserCO {
    @NotNull(message = "userco.username.null")
    @NotBlank(message = "userco.username.blank")
    private String username;

    @NotNull(message = "userco.password.null")
    @NotBlank(message = "userco.password.blank")
    @Size(min = 4,max = 100,message = "userco.password.size")
    private String password;

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public CoreRetrieveUserCO getCoreRetrieveUserCO(){
        CoreRetrieveUserCO coreRetrieveUserCO=new CoreRetrieveUserCO();
        coreRetrieveUserCO.setPassword(this.password);
        coreRetrieveUserCO.setUsername(this.username);
        coreRetrieveUserCO.setLoginRequest(true);
        return coreRetrieveUserCO;
    }
}
