package com.mexchange.rest.util.co.adressContact;


import com.mexchange.core.utils.co.addressContact.CoreCreateAddressCO;
import org.hibernate.validator.constraints.NotBlank;

import javax.validation.constraints.NotNull;

public class CreateAddressCO {

    @NotNull
    @NotBlank
    private String location;

    @NotNull
    @NotBlank
    private String latitude;

    @NotNull
    @NotBlank
    private String longitude;

    @NotNull
    @NotBlank
    private String country;

    private String state;

    private String city;

    private String pinCode;

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public String getLatitude() {
        return latitude;
    }

    public void setLatitude(String latitude) {
        this.latitude = latitude;
    }

    public String getLongitude() {
        return longitude;
    }

    public void setLongitude(String longitude) {
        this.longitude = longitude;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getPinCode() {
        return pinCode;
    }

    public void setPinCode(String pinCode) {
        this.pinCode = pinCode;
    }

    public CoreCreateAddressCO getCoreCreateAddressCO (){
        CoreCreateAddressCO coreCreateAddressCO=new CoreCreateAddressCO();
        coreCreateAddressCO.setLatitude(this.getLatitude());
        coreCreateAddressCO.setLongitude(this.getLongitude());
        coreCreateAddressCO.setLocation(this.getLocation());
        coreCreateAddressCO.setCountry(this.getCountry());
        coreCreateAddressCO.setState(this.getState());
        coreCreateAddressCO.setPinCode(this.getPinCode());
        coreCreateAddressCO.setCity(this.getCity());
        return coreCreateAddressCO;
    }
}
