//package com.mexchange.rest.util;
//
//import com.mexchange.core.exception.rest.*;
//import com.mexchange.rest.util.dto.ResponseDTO;
//import org.springframework.http.HttpStatus;
//import org.springframework.http.ResponseEntity;
//import org.springframework.web.bind.annotation.ControllerAdvice;
//import org.springframework.web.bind.annotation.ExceptionHandler;
//
//@ControllerAdvice(basePackages = {"com.mexchange.rest"})
//public class MExchangeControllerAdvice {
//
//    @ExceptionHandler(InternalServerException.class)
//    public ResponseEntity<ResponseDTO> internalServer(InternalServerException ex){
//        System.out.println(ex.getMessage());
//
//        ResponseDTO responseDTO=new ResponseDTO("500",ex.getMessage() !=null ? ex.getMessage():"Something serious went wrong");
//        return new ResponseEntity<ResponseDTO>(responseDTO, HttpStatus.INTERNAL_SERVER_ERROR);
//    }
//
//    @ExceptionHandler(UnauthorizedException.class)
//    public ResponseEntity<ResponseDTO> unauthorized(UnauthorizedException ex){
//        System.out.println(ex.getMessage());
//
//        ResponseDTO responseDTO=new ResponseDTO("401",ex.getMessage() !=null ? ex.getMessage():"Provided Username and password combination don't match");
//        return new ResponseEntity<ResponseDTO>(responseDTO, HttpStatus.UNAUTHORIZED);
//    }
//
//    @ExceptionHandler(NotFoundException.class)
//    public ResponseEntity<ResponseDTO> notFound(NotFoundException ex){
//        System.out.println(ex.getMessage());
//
//        ResponseDTO responseDTO=new ResponseDTO("404",ex.getMessage() !=null ? ex.getMessage():"Requested resource not found");
//        return new ResponseEntity<ResponseDTO>(responseDTO, HttpStatus.NOT_FOUND);
//    }
//
//    @ExceptionHandler(ForbiddenException.class)
//    public ResponseEntity<ResponseDTO> forbidden(ForbiddenException ex){
//        System.out.println(ex.getMessage());
//
//        ResponseDTO responseDTO=new ResponseDTO("403",ex.getMessage() !=null ? ex.getMessage():"Not allowed to access the requested resource");
//        return new ResponseEntity<ResponseDTO>(responseDTO, HttpStatus.FORBIDDEN);
//    }
//
//    @ExceptionHandler(MissingFieldException.class)
//    public ResponseEntity<ResponseDTO> missingField(MissingFieldException ex){
//        System.out.println(ex.getMessage());
//
//        ResponseDTO responseDTO=new ResponseDTO("422",ex.getMessage() !=null ? ex.getMessage():"Can't be processed as some required information is missing");
//        return new ResponseEntity<ResponseDTO>(responseDTO, HttpStatus.UNPROCESSABLE_ENTITY);
//    }
//
//    @ExceptionHandler(Exception.class)
//    public ResponseEntity<ResponseDTO> baseException(Exception ex){
//        System.out.println(ex.getMessage());
//        ResponseDTO responseDTO=new ResponseDTO("500","Something serious went wrong");
//        return new ResponseEntity<ResponseDTO>(responseDTO, HttpStatus.INTERNAL_SERVER_ERROR);
//    }
//}
