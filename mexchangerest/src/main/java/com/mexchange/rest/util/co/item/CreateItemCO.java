package com.mexchange.rest.util.co.item;

import com.mexchange.core.domain.files.SupportingImage;
import com.mexchange.core.utils.co.item.CoreCreateItemCO;
import com.mexchange.core.utils.enums.ItemType;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by amitsethi on 3/14/18.
 */
public class CreateItemCO {
    private String name;
    private Long quantity=1L;
    private ItemType itemType;
    private List<SupportingImage> images=new ArrayList<SupportingImage>();
    private String itemDescription;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Long getQuantity() {
        return quantity;
    }

    public void setQuantity(Long quantity) {
        this.quantity = quantity;
    }

    public ItemType getItemType() {
        return itemType;
    }

    public void setItemType(ItemType itemType) {
        this.itemType = itemType;
    }

    public List<SupportingImage> getImages() {
        return images;
    }

    public void setImages(List<SupportingImage> images) {
        this.images = images;
    }

    public String getItemDescription() {
        return itemDescription;
    }

    public void setItemDescription(String itemDescription) {
        this.itemDescription = itemDescription;
    }

    public CoreCreateItemCO getCoreCreateItemCO(){
        CoreCreateItemCO coreCreateItemCO=new CoreCreateItemCO();
        coreCreateItemCO.setName(this.name);
        coreCreateItemCO.setQuantity(this.quantity);
        coreCreateItemCO.setItemType(this.itemType);
        coreCreateItemCO.setItemDescription(this.itemDescription);
        coreCreateItemCO.setImages(this.images);
        return coreCreateItemCO;
    }
}
