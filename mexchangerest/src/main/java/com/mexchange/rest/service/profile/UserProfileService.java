package com.mexchange.rest.service.profile;


import com.mexchange.rest.util.co.profile.GetProfileDetailsCO;
import com.mexchange.rest.util.dto.ResponseDTO;
import org.springframework.mobile.device.Device;
import org.springframework.validation.BindingResult;

public interface UserProfileService {
    ResponseDTO getUserProfileDetails(GetProfileDetailsCO getProfileDetailsCO, BindingResult result,Device device);
}
