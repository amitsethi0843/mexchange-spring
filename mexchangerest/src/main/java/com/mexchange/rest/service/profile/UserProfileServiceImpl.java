package com.mexchange.rest.service.profile;

import com.mexchange.core.exception.MExchangeException;
import com.mexchange.core.exception.rest.InternalServerException;
import com.mexchange.core.exception.rest.MissingFieldException;
import com.mexchange.core.service.user.CoreUserService;
import com.mexchange.core.utils.AppUtil;
import com.mexchange.core.utils.co.CoreUserDetailsCO;
import com.mexchange.core.utils.vo.CoreUserDetailsVO;
import com.mexchange.rest.util.co.profile.GetProfileDetailsCO;
import com.mexchange.rest.util.dto.ResponseDTO;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mobile.device.Device;
import org.springframework.stereotype.Service;
import org.springframework.validation.BindingResult;

@Service("userProfileService")
public class UserProfileServiceImpl implements UserProfileService {

    @Autowired
    CoreUserService coreUserService;

    private Logger logger= AppUtil.getLogger(this.getClass());

    @Override
    public ResponseDTO getUserProfileDetails(GetProfileDetailsCO getProfileDetailsCO,BindingResult result, Device device) {
        ResponseDTO dto=new ResponseDTO();
        if (!result.hasErrors()) {
            CoreUserDetailsVO coreUserDetailsVO = coreUserService.getUserDetails(getProfileDetailsCO.getUserDetailsCO());
             if(coreUserDetailsVO.getUserDetailsList().size() == 1){
                dto.createSuccessResponseDTOInstance(coreUserDetailsVO,"success",null);
            }
            else{
                logger.error(MExchangeException.getFormattedError("Exception occured while fetching user profile details","Multiple users found with the same username",this.getClass().getEnclosingMethod().getName()));
                throw new InternalServerException("two users found with the same username");
            }
        }
        else{
            throw new MissingFieldException("username is required");
        }
        return dto;
    }
}
