package com.mexchange.rest.service.user;

import com.mexchange.core.domain.User;
import com.mexchange.core.exception.MExchangeException;
import com.mexchange.core.exception.rest.AlreadyPresentException;
import com.mexchange.core.exception.rest.MissingFieldException;
import com.mexchange.core.exception.rest.UnauthorizedException;
import com.mexchange.core.repository.AuthorityRepository;
import com.mexchange.core.repository.UserRepository;
import com.mexchange.core.service.user.CoreUserService;
import com.mexchange.core.utils.AppUtil;
import com.mexchange.rest.service.JwtAuthService;
import com.mexchange.rest.util.co.JwtAuthenticationRequest;
import com.mexchange.rest.util.co.user.CreateUserCO;
import com.mexchange.rest.util.co.user.LoginUserCO;
import com.mexchange.rest.util.dto.ResponseDTO;
import com.mexchange.rest.util.vo.JwtAuthenticationResponse;
import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.mobile.device.Device;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.validation.BindingResult;

import java.util.Locale;

@Service("userAccountService")
public class UserServiceImpl implements UserService {

    @Autowired
    UserRepository userRepository;

    @Autowired
    AuthorityRepository authorityRepository;

    @Autowired
    CoreUserService coreUserService;

    @Autowired
    JwtAuthService jwtAuthService;

    @Autowired
    private PasswordEncoder passwordEncoder;

    @Autowired
    private MessageSource messageSource;

    private Logger logger= AppUtil.getLogger(this.getClass());

    @Override
    public ResponseDTO logoutUser() {
        ResponseDTO dto=new ResponseDTO();
        System.out.println(SecurityContextHolder.getContext().getAuthentication());
        if(SecurityContextHolder.getContext().getAuthentication() != null) {
            System.out.println(SecurityContextHolder.getContext().getAuthentication());
            SecurityContextHolder.getContext().setAuthentication(null);
            System.out.println(SecurityContextHolder.getContext().getAuthentication());
            dto.createSuccessResponseDTOInstance(null,messageSource.getMessage("user.loggedOut",null, Locale.US),"201" );
        }
        return dto;
    }

    @Override
    public ResponseDTO create(CreateUserCO userCO, BindingResult result, Device device) {
        ResponseDTO dto=new ResponseDTO();
        JwtAuthenticationResponse jwtAuthenticationResponse;
        if (!result.hasErrors()) {
            try {
                 User user=coreUserService.createUser(userCO.getCoreUserCO());
                 jwtAuthenticationResponse = jwtAuthService.createToken(new JwtAuthenticationRequest(user.getUsername(), userCO.getPassword()), device);
                 dto.createSuccessResponseDTOInstance(jwtAuthenticationResponse,messageSource.getMessage("user.registered",null, Locale.US),"201" );
            }
            catch (AlreadyPresentException apex){
                logger.error(MExchangeException.getFormattedError("Cannot register user a duplicate user with the same credentials is already present",apex));
                throw new AlreadyPresentException(messageSource.getMessage("already.present.exception",null, Locale.US));
            }
            catch (Exception ex){
                logger.error(MExchangeException.getFormattedError("Error while registering user",ex));
                throw new RuntimeException(messageSource.getMessage("general.server.exception",null, Locale.US));
            }
        }
        else{
            throw new MissingFieldException(new MExchangeException().getFieldErrors(result));
        }
        return dto;
    }

    @Override
    public ResponseDTO login(LoginUserCO loginUserCO, BindingResult result, Device device){
        ResponseDTO dto=new ResponseDTO();
        JwtAuthenticationResponse jwtAuthenticationResponse;
        if (!result.hasErrors()) {
            try {
                User user=coreUserService.retrieveUser(loginUserCO.getCoreRetrieveUserCO());
                    if(user!=null) {
                        jwtAuthenticationResponse = jwtAuthService.createToken(new JwtAuthenticationRequest(loginUserCO.getUsername(), loginUserCO.getPassword()), device);
                        dto.createSuccessResponseDTOInstance(jwtAuthenticationResponse,messageSource.getMessage("user.loggedIn",null, Locale.US),"201" );
                    }
                    else {
                        throw new UnauthorizedException(messageSource.getMessage("credentials.not.valid",null, Locale.US));
                    }
                }
                catch (UnauthorizedException ex){
                    logger.error(MExchangeException.getFormattedError("Error while logging in user",ex));
                    throw new UnauthorizedException(messageSource.getMessage("credentials.not.valid",null, Locale.US));
                }
                catch (Exception ex){
                    logger.error(MExchangeException.getFormattedError("Error while logging in user",ex));
                    throw new RuntimeException(messageSource.getMessage("general.server.exception",null, Locale.US));
                }

            }else{
                throw new MissingFieldException(new MExchangeException().getFieldErrors(result));
            }
        return dto;
    }




    private void getUserProperties(String username){

    }
}
