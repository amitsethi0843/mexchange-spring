package com.mexchange.rest.service.user;

import com.mexchange.rest.util.co.user.CreateUserCO;
import com.mexchange.rest.util.co.user.LoginUserCO;
import com.mexchange.rest.util.dto.ResponseDTO;
import org.springframework.mobile.device.Device;
import org.springframework.validation.BindingResult;

public interface UserService {
    ResponseDTO create(CreateUserCO userCO, BindingResult result, Device device);
    ResponseDTO login(LoginUserCO loginUserCO, BindingResult result, Device device);
    ResponseDTO logoutUser();
}
