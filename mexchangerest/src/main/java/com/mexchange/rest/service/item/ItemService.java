package com.mexchange.rest.service.item;

import com.mexchange.core.domain.item.Item;
import com.mexchange.rest.util.co.item.CreateItemCO;
import com.mexchange.rest.util.dto.ResponseDTO;
import org.springframework.mobile.device.Device;
import org.springframework.validation.BindingResult;

/**
 * Created by amitsethi on 3/14/18.
 */
public interface ItemService {
    Item createItem(CreateItemCO createItemCO, Device device);
}
