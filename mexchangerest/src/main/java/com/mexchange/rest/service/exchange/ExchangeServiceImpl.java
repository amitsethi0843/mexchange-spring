package com.mexchange.rest.service.exchange;

import com.mexchange.core.domain.address.Address;
import com.mexchange.core.domain.contact.Contact;
import com.mexchange.core.domain.exchange.BuyerExchangeInstance;
import com.mexchange.core.domain.exchange.ExchangeInstance;
import com.mexchange.core.domain.exchange.SellerExchangeInstance;
import com.mexchange.core.domain.item.Item;
import com.mexchange.core.exception.MExchangeException;
import com.mexchange.core.exception.rest.MissingFieldException;
import com.mexchange.core.service.exchange.CoreExchangeService;
import com.mexchange.core.service.general.CoreAddressContactService;
import com.mexchange.core.utils.co.exchange.CoreCreateBuyerExchangeCO;
import com.mexchange.core.utils.co.exchange.CoreCreateExchangeCO;
import com.mexchange.core.utils.co.exchange.CoreCreateSellerExchangeCO;
import com.mexchange.rest.service.general.AddressContactService;
import com.mexchange.rest.service.item.ItemService;
import com.mexchange.rest.util.co.CreateExchangeCO;
import com.mexchange.rest.util.vo.CreateExchangeVO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mobile.device.Device;
import org.springframework.stereotype.Service;
import org.springframework.validation.BindingResult;

import com.mexchange.rest.util.dto.ResponseDTO;

@Service("exchangeService")
public class ExchangeServiceImpl implements ExchangeService {

	@Autowired
	private ItemService itemService;

	@Autowired
	private CoreExchangeService coreExchangeService;

	@Autowired
	private AddressContactService addressContactService;

	@Override
	public ResponseDTO create(CreateExchangeCO createExchangeCO, BindingResult result, Device device) {
		ResponseDTO responseDTO=new ResponseDTO();
		if(!result.hasErrors()){
			Item item= itemService.createItem(createExchangeCO.getCreateItemCO(),device);
			Address address=addressContactService.createAddress(createExchangeCO.getCreateAddressCO());
			createExchangeCO.setAddress(address);
			Contact contact=addressContactService.createContact(createExchangeCO.getCreateContactCO());
			createExchangeCO.setContact(contact);
			CoreCreateSellerExchangeCO coreCreateSellerExchangeCO=new CoreCreateSellerExchangeCO(item,createExchangeCO.getUsername());
			SellerExchangeInstance sellerExchangeInstance=coreExchangeService.createSellerExchange(coreCreateSellerExchangeCO);
			CoreCreateExchangeCO coreCreateExchangeCO=new CoreCreateExchangeCO(sellerExchangeInstance,address,contact);
			ExchangeInstance exchangeInstance=coreExchangeService.createExchange(coreCreateExchangeCO);
			responseDTO.createSuccessResponseDTOInstance(new CreateExchangeVO(exchangeInstance),"successfully created","201");
 		}else
		{
			throw new MissingFieldException(new MExchangeException().getFieldErrors(result));
		}
		return responseDTO;
	}

}
