package com.mexchange.rest.service.item;


import com.mexchange.core.domain.item.Item;
import com.mexchange.core.service.item.CoreItemService;
import com.mexchange.core.utils.co.item.CoreCreateItemCO;
import com.mexchange.rest.util.co.item.CreateItemCO;
import com.mexchange.rest.util.dto.ResponseDTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mobile.device.Device;
import org.springframework.stereotype.Service;

@Service("itemService")
public class ItemServiceImpl implements ItemService {

    @Autowired
    CoreItemService coreItemService;

    @Override
    public Item createItem(CreateItemCO createItemCO, Device device) {
        Item item=coreItemService.createItem(createItemCO.getCoreCreateItemCO(),device);
        return item;
    }
}
