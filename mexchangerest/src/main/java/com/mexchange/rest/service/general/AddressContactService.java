package com.mexchange.rest.service.general;


import com.mexchange.core.domain.address.Address;
import com.mexchange.core.domain.contact.Contact;
import com.mexchange.rest.util.co.adressContact.CreateAddressCO;
import com.mexchange.rest.util.co.adressContact.CreateContactCO;

public interface AddressContactService {

    Contact createContact(CreateContactCO createContactCO);
    
    Address createAddress(CreateAddressCO createAddressCO);
}
