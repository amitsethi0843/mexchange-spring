package com.mexchange.rest.service;


import com.mexchange.rest.auth.JwtTokenUtil;
import com.mexchange.rest.auth.JwtUser;
import com.mexchange.core.exception.rest.ForbiddenException;
import com.mexchange.rest.util.co.JwtAuthenticationRequest;
import com.mexchange.rest.util.vo.JwtAuthenticationResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mobile.device.Device;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;

@Service("jwtAuthService")
public class JwtAuthServiceImpl implements JwtAuthService {

    @Autowired
    private AuthenticationManager authenticationManager;

    @Autowired
    private JwtTokenUtil jwtTokenUtil;

    @Override
    public JwtAuthenticationResponse createToken(JwtAuthenticationRequest jwtAuthenticationRequest, Device device) {


        final Authentication authentication = authenticationManager.authenticate(
                new UsernamePasswordAuthenticationToken(
                        jwtAuthenticationRequest.getUsername(),
                        jwtAuthenticationRequest.getPassword()
                )
        );
        SecurityContextHolder.getContext().setAuthentication(authentication);
        if(authentication!=null){
            JwtUser jwtUser=(JwtUser) authentication.getPrincipal();
            final String token = jwtTokenUtil.generateToken(jwtUser, device);
            return new JwtAuthenticationResponse(token,jwtUser.getUsername(),jwtUser.getFirstname(),jwtUser.getLastname());
        }else{
            throw new ForbiddenException(null);
        }





        // Return the token
//        return ResponseEntity.ok(new JwtAuthenticationResponse(token));
    }
}
