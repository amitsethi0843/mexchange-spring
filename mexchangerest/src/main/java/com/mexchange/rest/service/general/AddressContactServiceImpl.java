package com.mexchange.rest.service.general;

import com.mexchange.core.domain.address.Address;
import com.mexchange.core.domain.contact.Contact;
import com.mexchange.core.service.general.CoreAddressContactService;
import com.mexchange.rest.util.co.adressContact.CreateAddressCO;
import com.mexchange.rest.util.co.adressContact.CreateContactCO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service("addressContactService")
public class AddressContactServiceImpl implements AddressContactService {

    @Autowired
    CoreAddressContactService coreAddressContactService;

    @Override
    public Contact createContact(CreateContactCO createContactCO) {
        return coreAddressContactService.createContact(createContactCO.getCoreCreateContactCO());
    }

    @Override
    public Address createAddress(CreateAddressCO createAddressCO) {
        return coreAddressContactService.createAddress(createAddressCO.getCoreCreateAddressCO());
    }
}
