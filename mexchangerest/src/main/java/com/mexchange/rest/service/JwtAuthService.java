package com.mexchange.rest.service;

import com.mexchange.rest.util.co.JwtAuthenticationRequest;
import com.mexchange.rest.util.vo.JwtAuthenticationResponse;
import org.springframework.mobile.device.Device;

public interface JwtAuthService {

    JwtAuthenticationResponse createToken(JwtAuthenticationRequest jwtAuthenticationRequest, Device device);
}
