package com.mexchange.rest.service.exchange;

import com.mexchange.rest.util.co.CreateExchangeCO;
import org.springframework.mobile.device.Device;
import org.springframework.validation.BindingResult;

import com.mexchange.rest.util.dto.ResponseDTO;

public interface ExchangeService {
	ResponseDTO create(CreateExchangeCO createExchangeCO, BindingResult result, Device device);
}
