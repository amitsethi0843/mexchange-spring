package com.mexchange.rest.controller.user;

import com.mexchange.rest.controller.BaseController;

import com.mexchange.rest.service.user.UserService;
import com.mexchange.rest.util.co.user.LoginUserCO;
import com.mexchange.rest.util.co.user.CreateUserCO;
import com.mexchange.rest.util.functionalInterface.ResponseInterface;
import com.mexchange.core.domain.User;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.mobile.device.Device;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

@RestController
@RequestMapping(value = "api/v1/user")
public class AccountController extends BaseController {


    @Autowired
    private UserService userAccountService;



    @Autowired
    private UserDetailsService userDetailsService;

    @RequestMapping(value = "/register",produces = MediaType.APPLICATION_JSON_VALUE,method = RequestMethod.POST)
    public Object create(@Valid @RequestBody CreateUserCO createUserCO, BindingResult bindingResult, Device device){
        ResponseInterface response=()-> userAccountService.create(createUserCO,bindingResult,device);
        return generateResponse(response);
    }

    @RequestMapping(value = "/login",produces = MediaType.APPLICATION_JSON_VALUE)
    public Object retrieve(@Valid @RequestBody LoginUserCO loginUserCO, BindingResult result, Device device){
        ResponseInterface response=()-> userAccountService.login(loginUserCO,result,device);
        return generateResponse(response);
    }

    @GetMapping(value = "/authenticate")
    public User getAuthenticatedUser(HttpServletRequest request) {
        String username=getUserName(request);
        User user = (User) userDetailsService.loadUserByUsername(username);
        return user;
    }

    @GetMapping(value = "/logout")
    public Object logout(){
        ResponseInterface response=()->userAccountService.logoutUser();
        return generateResponse(response);
    }


}
