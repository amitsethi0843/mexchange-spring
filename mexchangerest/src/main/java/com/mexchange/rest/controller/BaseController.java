package com.mexchange.rest.controller;



import com.mexchange.core.exception.rest.*;
import com.mexchange.rest.auth.JwtTokenUtil;
import com.mexchange.rest.util.dto.ResponseDTO;
import com.mexchange.rest.util.functionalInterface.ResponseInterface;
import com.mexchange.core.exception.MExchangeException;
import com.mexchange.core.utils.AppUtil;
import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import javax.servlet.http.HttpServletRequest;

public class BaseController {

    @Value("${jwt.header}")
    private String tokenHeader;

    @Autowired
    private JwtTokenUtil jwtTokenUtil;

    protected Logger logger= AppUtil.getLogger(this.getClass());

    protected ResponseEntity<ResponseDTO> generateResponse(ResponseInterface response) {
        ResponseDTO responseDTO;
        ResponseEntity responseEntity;
        try {
            responseDTO=(ResponseDTO) response.processResponse();
            responseEntity=new ResponseEntity<ResponseDTO>(responseDTO, HttpStatus.OK);
        }
        catch (UnauthorizedException ex){
            logger.error(MExchangeException.getFormattedError("Unauthorised exception",ex));
            responseDTO=new ResponseDTO(HttpStatus.UNAUTHORIZED.toString(),ex.getMessage() !=null ? ex.getMessage():"Unauthorised exception");
            responseEntity =new ResponseEntity<ResponseDTO>(responseDTO, HttpStatus.UNAUTHORIZED);
        }
        catch (NotFoundException ex){
            logger.error(MExchangeException.getFormattedError("Requested resource not found",ex));
            responseDTO=new ResponseDTO(HttpStatus.NOT_FOUND.toString(),ex.getMessage() !=null ? ex.getMessage():"Requested resource not found");
            responseEntity =new ResponseEntity<ResponseDTO>(responseDTO, HttpStatus.NOT_FOUND);
        }
        catch (ForbiddenException ex){
            logger.error(MExchangeException.getFormattedError("Not allowed to access the requested resource",ex));
            responseDTO=new ResponseDTO(HttpStatus.UNPROCESSABLE_ENTITY.toString(),ex.getMessage() !=null ? ex.getMessage():"Not allowed to access the requested resource");
            responseEntity =new ResponseEntity<ResponseDTO>(responseDTO, HttpStatus.UNPROCESSABLE_ENTITY);
        }
        catch (MissingFieldException ex){
            logger.error(MExchangeException.getFormattedError("Can't be processed as some required information is missing",ex));
            responseDTO=new ResponseDTO(HttpStatus.FORBIDDEN.toString(),ex.getMessage() !=null ? ex.getMessage():"Can't be processed as some required information is missing");
            responseEntity =new ResponseEntity<ResponseDTO>(responseDTO, HttpStatus.FORBIDDEN);
        }
        catch (AlreadyPresentException ex){
            logger.error(MExchangeException.getFormattedError("Can't be processed as duplicate resource is already present",ex));
            responseDTO=new ResponseDTO(HttpStatus.FORBIDDEN.toString(),ex.getMessage() !=null ? ex.getMessage():"Can't be processed as duplicate resource is already present");
            responseEntity =new ResponseEntity<ResponseDTO>(responseDTO, HttpStatus.CONFLICT);
        }
        catch (Exception ex){
            logger.error(MExchangeException.getFormattedError("Internal server error",ex));
            responseDTO=new ResponseDTO("500",ex.getMessage() !=null ? ex.getMessage():"Something serious went wrong");
            responseEntity =new ResponseEntity<ResponseDTO>(responseDTO, HttpStatus.INTERNAL_SERVER_ERROR);
        }

        return responseEntity;
    }

    protected  String extractAuthToken(HttpServletRequest request) {
        return request.getHeader("x-auth-token");
    }

    protected String getUserName(HttpServletRequest request){
        String token = request.getHeader(tokenHeader).substring(7);
        String username = jwtTokenUtil.getUsernameFromToken(token);
        return username;
    }

}
