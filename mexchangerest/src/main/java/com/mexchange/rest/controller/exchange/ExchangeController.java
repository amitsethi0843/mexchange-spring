package com.mexchange.rest.controller.exchange;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

import com.mexchange.rest.util.co.CreateExchangeCO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mobile.device.Device;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.mexchange.rest.controller.BaseController;
import com.mexchange.rest.service.exchange.ExchangeService;
import com.mexchange.rest.util.functionalInterface.ResponseInterface;

@RestController
@RequestMapping(value = "api/v1/exchange")
public class ExchangeController extends BaseController {
	
	@Autowired
	private ExchangeService exchangeService;

	@PostMapping(value="/create")
	public Object create(@RequestBody CreateExchangeCO createExchangeCO, BindingResult result, Device device, HttpServletRequest request) {
		String username=getUserName(request);
		createExchangeCO.setUsername(username);
		ResponseInterface response=()-> exchangeService.create(createExchangeCO,result,device);
        return generateResponse(response);
	}
}
