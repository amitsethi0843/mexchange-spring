package com.mexchange.rest.controller.user.profile;

import com.mexchange.rest.controller.BaseController;
import com.mexchange.rest.service.profile.UserProfileService;
import com.mexchange.rest.util.co.profile.GetProfileDetailsCO;
import com.mexchange.rest.util.co.user.LoginUserCO;
import com.mexchange.rest.util.functionalInterface.ResponseInterface;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mobile.device.Device;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

@RestController
@RequestMapping(value = "api/v1/profile")
public class UserProfileController extends BaseController {

    @Autowired
    private UserProfileService userProfileService;

    @GetMapping(value = "/userDetails")
    public Object getUserDetails(@Valid @ModelAttribute GetProfileDetailsCO getProfileDetailsCO,BindingResult result,Device device){
        ResponseInterface response=()-> userProfileService.getUserProfileDetails(getProfileDetailsCO,result,device);
        return generateResponse(response);
    }

}
