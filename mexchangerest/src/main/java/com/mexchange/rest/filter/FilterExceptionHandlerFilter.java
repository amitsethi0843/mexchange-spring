package com.mexchange.rest.filter;

import com.mexchange.core.utils.AppUtil;
import com.mexchange.rest.util.dto.ResponseDTO;
import org.springframework.http.HttpStatus;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.web.filter.OncePerRequestFilter;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

public class FilterExceptionHandlerFilter extends OncePerRequestFilter {

    @Override
    public void doFilterInternal(HttpServletRequest request, HttpServletResponse response, FilterChain filterChain) throws ServletException, IOException {
        try {
            filterChain.doFilter(request, response);
        } catch (UsernameNotFoundException e) {
            // custom error response class used across my project
            ResponseDTO errorResponse = new ResponseDTO(null,"1001","User with the given token not found");
            response.setStatus(HttpStatus.FORBIDDEN.value());
            response.getWriter().write(AppUtil.convertObjectToJson(errorResponse));
        }
    }


}
