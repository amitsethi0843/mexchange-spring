package com.mexchange.core.exception;


import com.mexchange.core.utils.AppUtil;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.exception.ExceptionUtils;
import org.springframework.context.MessageSource;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.validation.ObjectError;

import java.util.HashSet;
import java.util.Locale;
import java.util.Set;

public class MExchangeException{


    private MessageSource messageSource= AppUtil.getMessageSource();


    public static String getFormattedError(String exception,Exception ex){
        return exception + " root cause: "+ ExceptionUtils.getRootCauseMessage(ex) + " message: "+ex.getMessage();
    }

    public static String getFormattedError(String exception,String ex,String method){
        return exception + " in method " + method + " root cause: "+ ex;
    }

    public String getFieldErrors(BindingResult result){
        Set<String> fieldErrors=new HashSet<String>();
        if(result.hasFieldErrors()){
            for(FieldError fieldError:result.getFieldErrors()){
                fieldErrors.add(this.messageSource.getMessage(fieldError.getDefaultMessage(),null, Locale.US));
            }
        }
        else{
            for(ObjectError error:result.getAllErrors()){
                fieldErrors.add(error.getDefaultMessage());
            }
        }
        return StringUtils.join(fieldErrors,",");
    }

//    public static void createOrResetErrorMap(int errorFlag,String errorCode,String errorText,Map<String,Object> body){
//        Map<String,Object> errorMap=new HashMap<String,Object>();
//        errorMap.put(CoreConstants.ERROR_FLAG,errorFlag);
//        errorMap.put(CoreConstants.ERROR_CODE,errorCode);
//        errorMap.put(CoreConstants.ERROR_TEXT,errorText);
//        body.put(CoreConstants.ERROR_MAP,errorMap);
//    }
//
//    public static  Map<String,Object> createBodyWithSuccessErrorMap(Map<String,Object> body){
//        Map<String,Object> errorMap=new HashMap<String,Object>();
//        errorMap.put(CoreConstants.ERROR_FLAG,0);
//        errorMap.put(CoreConstants.ERROR_CODE,"000");
//        errorMap.put(CoreConstants.ERROR_TEXT,"SUCCESS");
//        body.put(CoreConstants.ERROR_MAP,errorMap);
//        return body;
//    }
}
