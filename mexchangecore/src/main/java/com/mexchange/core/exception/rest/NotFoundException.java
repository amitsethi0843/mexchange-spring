package com.mexchange.core.exception.rest;

public class NotFoundException extends RuntimeException{

    NotFoundException(String message){
        super(message);
    }
}
