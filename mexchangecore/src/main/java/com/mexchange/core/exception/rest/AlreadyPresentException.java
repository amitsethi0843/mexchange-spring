package com.mexchange.core.exception.rest;


public class AlreadyPresentException extends RuntimeException {

    public AlreadyPresentException(String message){
        super(message);
    }

    public AlreadyPresentException(){

    }
}
