package com.mexchange.core.exception.rest;

public class InternalServerException extends RuntimeException {

    public InternalServerException(String message){
        super(message);
    }

}
