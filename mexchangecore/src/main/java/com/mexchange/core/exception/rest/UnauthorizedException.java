package com.mexchange.core.exception.rest;

public class UnauthorizedException extends RuntimeException {

    public UnauthorizedException(String message){
        super(message);
    }
    public UnauthorizedException(){

    }
}
