package com.mexchange.core.exception.rest;

public class MissingFieldException extends RuntimeException {

    public MissingFieldException(String message){
        super(message);
    }
}
