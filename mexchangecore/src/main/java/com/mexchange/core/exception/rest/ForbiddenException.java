package com.mexchange.core.exception.rest;

public class ForbiddenException extends RuntimeException{

   public ForbiddenException(String message){
        super(message);
    }

}
