package com.mexchange.core.repository;

import com.mexchange.core.domain.Authority;
import com.mexchange.core.domain.AuthorityName;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

@Repository
public interface AuthorityRepository extends JpaRepository<Authority,Long> {

    @Query("select a from Authority a where a.name = :name")
    Authority findByName(@Param("name") AuthorityName name);
}
