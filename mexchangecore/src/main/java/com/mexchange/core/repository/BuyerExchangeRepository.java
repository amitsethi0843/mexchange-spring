package com.mexchange.core.repository;

import com.mexchange.core.domain.exchange.BuyerExchangeInstance;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;


@Repository
public interface BuyerExchangeRepository extends JpaRepository<BuyerExchangeInstance,Long> {
}
