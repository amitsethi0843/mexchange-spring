package com.mexchange.core.repository;

import com.mexchange.core.domain.exchange.SellerExchangeInstance;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 * Created by amitsethi on 3/18/18.
 */

@Repository
public interface SellerExchangeRepository extends JpaRepository<SellerExchangeInstance,Long> {
}
