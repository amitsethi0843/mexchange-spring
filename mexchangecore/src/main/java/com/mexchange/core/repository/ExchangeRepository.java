package com.mexchange.core.repository;

import com.mexchange.core.domain.User;
import com.mexchange.core.domain.exchange.ExchangeInstance;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ExchangeRepository extends JpaRepository<ExchangeInstance,Long> {

    @Query("select ex from ExchangeInstance ex where ex.createdBy = :user")
    List<ExchangeInstance> findAllByCreatedBy(@Param("user") User user);
}
