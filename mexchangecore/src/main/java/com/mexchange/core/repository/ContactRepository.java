package com.mexchange.core.repository;

import com.mexchange.core.domain.contact.Contact;
import org.springframework.data.jpa.repository.JpaRepository;


public interface ContactRepository extends JpaRepository<Contact,Long> {
}
