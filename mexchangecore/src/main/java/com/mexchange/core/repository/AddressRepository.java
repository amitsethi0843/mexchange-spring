package com.mexchange.core.repository;

import com.mexchange.core.domain.address.Address;
import org.springframework.data.jpa.repository.JpaRepository;


public interface AddressRepository extends JpaRepository<Address,Long> {

}
