package com.mexchange.core.repository;

import com.mexchange.core.domain.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

@Repository
public interface UserRepository extends JpaRepository<User,Long> {

    @Query("select a from User a where a.username = :username")
    User findByUsername(@Param("username") String username);

}
