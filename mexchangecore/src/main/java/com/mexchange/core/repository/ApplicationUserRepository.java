package com.mexchange.core.repository;

import com.mexchange.core.domain.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ApplicationUserRepository extends JpaRepository<User,Long> {
    User findByUsername(String username);
}
