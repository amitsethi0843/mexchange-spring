package com.mexchange.core.service.exchange;

import com.mexchange.core.domain.User;
import com.mexchange.core.domain.exchange.BuyerExchangeInstance;
import com.mexchange.core.domain.exchange.ExchangeInstance;
import com.mexchange.core.domain.exchange.SellerExchangeInstance;
import com.mexchange.core.repository.BuyerExchangeRepository;
import com.mexchange.core.repository.ExchangeRepository;
import com.mexchange.core.repository.SellerExchangeRepository;
import com.mexchange.core.service.user.CoreUserService;
import com.mexchange.core.utils.co.CoreRetrieveUserCO;
import com.mexchange.core.utils.co.exchange.CoreCreateBuyerExchangeCO;
import com.mexchange.core.utils.co.exchange.CoreCreateExchangeCO;
import com.mexchange.core.utils.co.exchange.CoreCreateSellerExchangeCO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;


@Service("coreExchangeService")
public class CoreExchangeServiceImpl implements CoreExchangeService{

    @Autowired
    private BuyerExchangeRepository buyerExchangeRepository;

    @Autowired
    private SellerExchangeRepository sellerExchangeRepository;

    @Autowired
    private ExchangeRepository exchangeRepository;

    @Autowired
    private CoreUserService coreUserService;

    @Override
    public BuyerExchangeInstance createBuyerExchange(CoreCreateBuyerExchangeCO coreCreateBuyerExchangeCO) {
        CoreRetrieveUserCO coreRetrieveUserCO=new CoreRetrieveUserCO(coreCreateBuyerExchangeCO.getUsername());
        User user=coreUserService.retrieveUser(coreRetrieveUserCO);
        BuyerExchangeInstance buyerExchangeInstance=new BuyerExchangeInstance(coreCreateBuyerExchangeCO.getItems(),user);
        buyerExchangeInstance= buyerExchangeRepository.saveAndFlush(buyerExchangeInstance);
        return buyerExchangeInstance;
    }

    @Override
    public SellerExchangeInstance createSellerExchange(CoreCreateSellerExchangeCO coreCreateSellerExchangeCO) {
        CoreRetrieveUserCO coreRetrieveUserCO=new CoreRetrieveUserCO(coreCreateSellerExchangeCO.getUsername());
        User user=coreUserService.retrieveUser(coreRetrieveUserCO);
        SellerExchangeInstance sellerExchangeInstance=new SellerExchangeInstance(coreCreateSellerExchangeCO.getItem(),user);
        sellerExchangeInstance= sellerExchangeRepository.saveAndFlush(sellerExchangeInstance);
        return sellerExchangeInstance;
    }

    @Override
    public ExchangeInstance createExchange(CoreCreateExchangeCO coreCreateExchangeCO) {
        ExchangeInstance exchangeInstance=new ExchangeInstance(coreCreateExchangeCO);
        exchangeInstance=exchangeRepository.saveAndFlush(exchangeInstance);
        return exchangeInstance;
    }
}
