package com.mexchange.core.service.user;

import com.mexchange.core.domain.Authority;
import com.mexchange.core.domain.AuthorityName;
import com.mexchange.core.domain.User;
import com.mexchange.core.exception.rest.AlreadyPresentException;
import com.mexchange.core.exception.rest.UnauthorizedException;
import com.mexchange.core.repository.AuthorityRepository;
import com.mexchange.core.repository.UserRepository;
import com.mexchange.core.utils.co.CoreCreateUserCO;
import com.mexchange.core.utils.co.CoreRetrieveUserCO;
import com.mexchange.core.utils.co.CoreUserDetailsCO;
import com.mexchange.core.utils.vo.CoreUserDetailsVO;
import org.apache.commons.lang.StringUtils;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.criterion.Projection;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Path;
import javax.persistence.criteria.Root;
import java.util.Arrays;


@Service("coreUserService")
public class CoreUserServiceImpl implements CoreUserService {

    @Autowired
    private PasswordEncoder passwordEncoder;

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private AuthorityRepository authorityRepository;

    @PersistenceContext
    private EntityManager entityManager;



    @Override
    public User createUser(CoreCreateUserCO coreCreateUserCO) {
        User user=userRepository.findByUsername(coreCreateUserCO.getUsername());
        if(user!=null){
            throw new AlreadyPresentException();
        }
        user=new User(coreCreateUserCO);
        user.setPassword(passwordEncoder.encode(coreCreateUserCO.getPassword()));
        assignUserRole(user);
        user=userRepository.save(user);
        return user;
    }

    @Override
    public User retrieveUser(CoreRetrieveUserCO coreRetrieveUserCO) {
        User user = userRepository.findByUsername(coreRetrieveUserCO.getUsername());
        if (user != null && coreRetrieveUserCO.isLoginRequest()) {

            if (passwordEncoder.matches(coreRetrieveUserCO.getPassword(), user.getPassword())) {
                return user;
            } else {
                throw new UnauthorizedException();
            }

        }else{
            return user;
        }

    }

    private void assignUserRole(User user){
        Authority authority=authorityRepository.findByName(AuthorityName.ROLE_USER);
        if(authority==null){
            authority=new Authority(AuthorityName.ROLE_USER);
            authorityRepository.saveAndFlush(authority);
        }
        user.addToAuthorities(authority);
    }

    @Override
    public CoreUserDetailsVO getUserDetails(CoreUserDetailsCO coreUserDetailsCO) {
        Criteria crit = entityManager.unwrap(Session.class).createCriteria(User.class, "user");
        if (coreUserDetailsCO.getUsername() != null) {
            crit.add(Restrictions.eq("username", coreUserDetailsCO.getUsername()));
            if (StringUtils.isNotEmpty(coreUserDetailsCO.getParams())) {
                Arrays.stream(coreUserDetailsCO.getParams().split(",")).forEach(param -> {
                    if (param.equalsIgnoreCase("contact") || param.equalsIgnoreCase("contactnumber")) {
                        crit.createAlias("user.contact", "contact");
                        crit.setProjection(Projections.property("contact.contactNumber"));
                    } else {
                        crit.setProjection(Projections.property(param));
                    }
                });
            }
            CoreUserDetailsVO coreUserDetailsVO = new CoreUserDetailsVO(crit.list());
            return coreUserDetailsVO;
        }
        return null;
    }
}
