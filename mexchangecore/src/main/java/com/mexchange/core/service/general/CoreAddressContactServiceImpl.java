package com.mexchange.core.service.general;

import com.mexchange.core.domain.address.Address;
import com.mexchange.core.domain.contact.Contact;
import com.mexchange.core.utils.co.addressContact.CoreCreateAddressCO;
import com.mexchange.core.utils.co.addressContact.CoreCreateContactCO;
import org.springframework.stereotype.Service;

/**
 * Created by amitsethi on 4/1/18.
 */

@Service("coreAddressContactService")
public class CoreAddressContactServiceImpl implements CoreAddressContactService{

    @Override
    public Address createAddress(CoreCreateAddressCO coreCreateAddressContactCO) {
        return null;
    }

    @Override
    public Contact createContact(CoreCreateContactCO coreCreateAddressContactCO) {
        return null;
    }
}
