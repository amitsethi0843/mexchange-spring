package com.mexchange.core.service.general;

import com.mexchange.core.domain.address.Address;
import com.mexchange.core.domain.contact.Contact;
import com.mexchange.core.utils.co.addressContact.CoreCreateAddressCO;
import com.mexchange.core.utils.co.addressContact.CoreCreateContactCO;

public interface CoreAddressContactService {


    public Address createAddress(CoreCreateAddressCO coreCreateAddressContactCO);

    public Contact createContact(CoreCreateContactCO coreCreateAddressContactCO);

}
