package com.mexchange.core.service.user;


import com.mexchange.core.domain.User;
import com.mexchange.core.utils.co.CoreCreateUserCO;
import com.mexchange.core.utils.co.CoreRetrieveUserCO;
import com.mexchange.core.utils.co.CoreUserDetailsCO;
import com.mexchange.core.utils.vo.CoreUserDetailsVO;

public interface CoreUserService {

    User createUser(CoreCreateUserCO coreCreateUserCO);
    User retrieveUser(CoreRetrieveUserCO coreRetrieveUserCO);
    CoreUserDetailsVO getUserDetails(CoreUserDetailsCO coreUserDetailsCO);
}
