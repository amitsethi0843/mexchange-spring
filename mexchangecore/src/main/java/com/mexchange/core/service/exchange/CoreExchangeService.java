package com.mexchange.core.service.exchange;

import com.mexchange.core.domain.exchange.BuyerExchangeInstance;
import com.mexchange.core.domain.exchange.ExchangeInstance;
import com.mexchange.core.domain.exchange.SellerExchangeInstance;
import com.mexchange.core.utils.co.exchange.CoreCreateBuyerExchangeCO;
import com.mexchange.core.utils.co.exchange.CoreCreateExchangeCO;
import com.mexchange.core.utils.co.exchange.CoreCreateSellerExchangeCO;

/**
 * Created by amitsethi on 3/18/18.
 */
public interface CoreExchangeService {

    BuyerExchangeInstance createBuyerExchange(CoreCreateBuyerExchangeCO coreCreateBuyerExchangeCO);

    SellerExchangeInstance createSellerExchange(CoreCreateSellerExchangeCO coreCreateSellerExchangeCO);

    ExchangeInstance createExchange(CoreCreateExchangeCO coreCreateExchangeCO);
}
