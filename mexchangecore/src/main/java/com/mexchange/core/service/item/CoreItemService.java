package com.mexchange.core.service.item;


import com.mexchange.core.domain.item.Item;
import com.mexchange.core.utils.co.item.CoreCreateItemCO;
import org.springframework.mobile.device.Device;

public interface CoreItemService {
    Item createItem(CoreCreateItemCO coreCreateItemCO, Device device);
}
