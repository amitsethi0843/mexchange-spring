package com.mexchange.core.service.item;

import com.mexchange.core.domain.item.Item;
import com.mexchange.core.repository.ItemRepository;
import com.mexchange.core.utils.co.item.CoreCreateItemCO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mobile.device.Device;
import org.springframework.stereotype.Service;

@Service("coreItemService")
public class CoreItemServiceImpl implements CoreItemService {

    @Autowired
    ItemRepository itemRepository;

    @Override
    public Item createItem(CoreCreateItemCO coreCreateItemCO, Device device) {
        Item item=new Item(coreCreateItemCO);
        item=itemRepository.saveAndFlush(item);
        return item;
    }
}
