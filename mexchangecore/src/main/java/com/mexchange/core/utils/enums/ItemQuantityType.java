package com.mexchange.core.utils.enums;


public enum ItemQuantityType {

    UNIT("Unit"),
    KG("kg");

    String key;
    String value;
    ItemQuantityType(String value){
        this.value=value;
    }

    String getKey(){
        return this.name();
    }

    String getValue(){
        return this.value;
    }

}
