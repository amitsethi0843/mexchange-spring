package com.mexchange.core.utils.co;


import org.springframework.mobile.device.Device;

public class CoreCreateUserCO {
    private String username;
    private String firstName;
    private String lastName;
    private String password;
    private String cotactNumber;
    private String retypePassword;
    private Device device;

    public String getCotactNumber() {
        return cotactNumber;
    }

    public void setCotactNumber(String cotactNumber) {
        this.cotactNumber = cotactNumber;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getRetypePassword() {
        return retypePassword;
    }

    public void setRetypePassword(String retypePassword) {
        this.retypePassword = retypePassword;
    }

    public Device getDevice() {
        return device;
    }

    public void setDevice(Device device) {
        this.device = device;
    }
}
