package com.mexchange.core.utils.co.exchange;

import com.mexchange.core.domain.item.Item;

/**
 * Created by amitsethi on 3/18/18.
 */
public class CoreCreateSellerExchangeCO {
    private String username;

    private Item item;

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public Item getItem() {
        return item;
    }

    public void setItem(Item item) {
        this.item = item;
    }

    public CoreCreateSellerExchangeCO(){

    }

    public CoreCreateSellerExchangeCO(Item item,String username){
        this.item=item;
        this.username=username;
    }
}
