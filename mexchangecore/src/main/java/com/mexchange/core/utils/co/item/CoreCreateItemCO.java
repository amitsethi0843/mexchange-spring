package com.mexchange.core.utils.co.item;


import com.mexchange.core.domain.files.SupportingImage;
import com.mexchange.core.utils.enums.ItemType;

import java.util.ArrayList;
import java.util.List;

public class CoreCreateItemCO {

    private String name;
    private Long quantity=1L;
    private ItemType itemType;
    private List<SupportingImage> images=new ArrayList<SupportingImage>();
    private String itemDescription;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Long getQuantity() {
        return quantity;
    }

    public void setQuantity(Long quantity) {
        this.quantity = quantity;
    }

    public ItemType getItemType() {
        return itemType;
    }

    public void setItemType(ItemType itemType) {
        this.itemType = itemType;
    }






    public List<SupportingImage> getImages() {
        return images;
    }

    public void setImages(List<SupportingImage> images) {
        this.images = images;
    }

    public String getItemDescription() {
        return itemDescription;
    }

    public void setItemDescription(String itemDescription) {
        this.itemDescription = itemDescription;
    }

}
