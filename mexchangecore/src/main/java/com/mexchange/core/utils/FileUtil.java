package com.mexchange.core.utils;


public class FileUtil {

    public static String getExtension(String filename) {
        String extension=null;
        if (filename.indexOf(".")>0) {
            extension=filename.substring(filename.indexOf(".")+1,filename.length());
        }

        return extension;
    }

}
