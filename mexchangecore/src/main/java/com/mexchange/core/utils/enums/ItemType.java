package com.mexchange.core.utils.enums;

public enum ItemType {
    PRODUCT("Product"),
    CASH("Cash");

    String key;
    String value;
    ItemType(String value){
        this.value=value;
    }

    String getKey(){
        return this.name();
    }

    String getValue(){
        return this.value;
    }
}
