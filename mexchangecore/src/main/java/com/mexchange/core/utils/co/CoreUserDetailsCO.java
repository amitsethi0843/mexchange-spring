package com.mexchange.core.utils.co;

import org.apache.commons.lang.StringUtils;


/**
 * Created by amitsethi on 3/4/18.
 */
public class CoreUserDetailsCO {

    private String username;

    private String password;

    private String firstName;

    private String lastName;

    private String params;

    public String getParams() {
        return params;
    }

    public void setParams(String params) {
        this.params = params;
    }

    public String getUsername() {
        return username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public CoreUserDetailsCO(String username){
        this.username=username;
    }
}
