package com.mexchange.core.utils.enums;


public enum FileType {
    ITEMIMAGE("Item Image"),
    USERPROFILEIMAGE("User Profile Image");

    String value;

    FileType(String value){
        this.value=value;
    }

    String getValue(){
        return this.value;
    }

    String getKey(){
        return this.name();
    }
}
