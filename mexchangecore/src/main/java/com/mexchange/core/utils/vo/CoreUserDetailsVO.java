package com.mexchange.core.utils.vo;

import com.mexchange.core.domain.User;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by amitsethi on 3/4/18.
 */
public class CoreUserDetailsVO {

    private List<Object> userDetailsList=new ArrayList<Object>();

    public List<Object> getUserDetailsList() {
        return userDetailsList;
    }

    public void setUserDetailsList(List<Object> userDetailsList) {
        this.userDetailsList = userDetailsList;
    }

    public CoreUserDetailsVO(List userList){
        this.userDetailsList=userList;
    }
}
