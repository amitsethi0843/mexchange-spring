package com.mexchange.core.utils.co.exchange;

import com.mexchange.core.domain.User;
import com.mexchange.core.domain.item.Item;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by amitsethi on 3/18/18.
 */
public class CoreCreateBuyerExchangeCO {
    private String username;

    private List<Item> items=new ArrayList<Item>();

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public List<Item> getItems() {
        return items;
    }

    public void setItems(List<Item> items) {
        this.items = items;
    }
}
