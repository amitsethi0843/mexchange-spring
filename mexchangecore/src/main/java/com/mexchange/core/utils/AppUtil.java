package com.mexchange.core.utils;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeansException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.context.MessageSource;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Component;

import java.util.UUID;

@Component
public class AppUtil implements ApplicationContextAware {

    @Autowired
    private static ApplicationContext applicationContext;

    public static String getUniqueID() {
        return UUID.randomUUID().toString().replaceAll("-", "");
    }

    public static Logger getLogger(Class<?> classObj) {
        return LoggerFactory.getLogger(classObj);
    }

    private static Object getBean(String beanName){
        Object object = applicationContext.getBean(beanName);
        if(object!=null){
            return object;
        }
        else{
            return null;
        }
    }

    public static MessageSource getMessageSource(){
        return (MessageSource) getBean("messageSource");

    }

    public static Environment getConfig(){
        return applicationContext.getEnvironment();
    }

    @Override
    public void setApplicationContext(ApplicationContext context) throws BeansException {
        applicationContext=context;
    }

    public static String convertObjectToJson(Object object)throws JsonProcessingException {
        ObjectMapper mapper = new ObjectMapper();
        return mapper.writeValueAsString(object);
    }

    public static ApplicationContext getAppContext() {
        return applicationContext;
    }
}
