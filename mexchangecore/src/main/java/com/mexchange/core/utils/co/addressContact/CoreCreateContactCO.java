package com.mexchange.core.utils.co.addressContact;

/**
 * Created by amitsethi on 4/1/18.
 */
public class CoreCreateContactCO {

    private String contact;

    public String getContact() {
        return contact;
    }

    public void setContact(String contact) {
        this.contact = contact;
    }
}
