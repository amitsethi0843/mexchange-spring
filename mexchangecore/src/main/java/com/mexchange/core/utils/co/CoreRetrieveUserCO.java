package com.mexchange.core.utils.co;


public class CoreRetrieveUserCO {
    private String username;
    private String password;
    private boolean isLoginRequest=true;

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public boolean isLoginRequest() {
        return isLoginRequest;
    }

    public void setLoginRequest(boolean loginRequest) {
        isLoginRequest = loginRequest;
    }

    public CoreRetrieveUserCO(String username){
        this.username=username;
        this.isLoginRequest=false;
    }

    public CoreRetrieveUserCO(){}
}
