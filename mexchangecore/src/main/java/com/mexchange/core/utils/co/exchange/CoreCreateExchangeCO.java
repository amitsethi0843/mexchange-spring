package com.mexchange.core.utils.co.exchange;

import com.mexchange.core.domain.address.Address;
import com.mexchange.core.domain.contact.Contact;
import com.mexchange.core.domain.exchange.BuyerExchangeInstance;
import com.mexchange.core.domain.exchange.SellerExchangeInstance;

/**
 * Created by amitsethi on 3/18/18.
 */
public class CoreCreateExchangeCO {

    private SellerExchangeInstance sellerExchangeInstance;

    private Address address;

    private Contact contact;

    public Address getAddress() {
        return address;
    }

    public void setAddress(Address address) {
        this.address = address;
    }

    public Contact getContact() {
        return contact;
    }

    public void setContact(Contact contact) {
        this.contact = contact;
    }

    public SellerExchangeInstance getSellerExchangeInstance() {
        return sellerExchangeInstance;
    }

    public void setSellerExchangeInstance(SellerExchangeInstance sellerExchangeInstance) {
        this.sellerExchangeInstance = sellerExchangeInstance;
    }

    public CoreCreateExchangeCO(){}

    public CoreCreateExchangeCO (SellerExchangeInstance sellerExchangeInstance,Address address,Contact contact){
        this.sellerExchangeInstance=sellerExchangeInstance;
        this.address=address;
        this.contact=contact;
    }
}
