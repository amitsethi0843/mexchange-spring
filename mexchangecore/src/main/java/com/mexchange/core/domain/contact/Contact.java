package com.mexchange.core.domain.contact;


import com.mexchange.core.domain.BaseEntity;
import com.mexchange.core.utils.AppUtil;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

@Entity
@Table(name = "CONTACT")
public class Contact extends BaseEntity {

    @NotNull
    @Column(name = "CONTACT_NUMBER")
    private String contactNumber;

    public String getContactNumber() {
        return contactNumber;
    }

    public void setContactNumber(String contactNumber) {
        this.contactNumber = contactNumber;
    }

    public Contact(){

    }

    public Contact(String contactNumber){
        this.contactNumber=contactNumber;
        this.setUuid(AppUtil.getUniqueID());
    }
}
