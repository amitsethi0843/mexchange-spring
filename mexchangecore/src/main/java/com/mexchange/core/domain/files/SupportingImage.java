package com.mexchange.core.domain.files;


import com.mexchange.core.domain.BaseEntity;
import com.mexchange.core.utils.AppUtil;
import com.mexchange.core.utils.FileUtil;
import com.mexchange.core.utils.enums.FileType;
import org.springframework.web.multipart.MultipartFile;

import javax.persistence.*;
import javax.validation.constraints.NotNull;

@Entity
@Table(name = "SUPPORTING_IMAGE")
public class SupportingImage extends BaseEntity {

    @NotNull
    @Column(name = "NAME")
    private String name;

    @NotNull
    @Column(name = "PATH")
    private String path;

    @Column(name = "INFO")
    private String info;

    @NotNull
    @Column(name = "CONTENT_TYPE")
    private String contentType;

    @NotNull
    @Column(name = "EXTENSION")
    private String extension;

    @NotNull
    @Enumerated(EnumType.STRING)
    private FileType fileType;

    SupportingImage(MultipartFile multipartFile,String path,FileType fileType,String name){
        this.name=multipartFile.getOriginalFilename();
        this.setUuid(AppUtil.getUniqueID());
        this.fileType=fileType;
        this.extension= FileUtil.getExtension(this.name);
        this.contentType=multipartFile.getContentType();
        this.path = path + "/"+this.name;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }

    public String getInfo() {
        return info;
    }

    public void setInfo(String info) {
        this.info = info;
    }

    public String getContentType() {
        return contentType;
    }

    public void setContentType(String contentType) {
        this.contentType = contentType;
    }

    public String getExtension() {
        return extension;
    }

    public void setExtension(String extension) {
        this.extension = extension;
    }

    public FileType getFileType() {
        return fileType;
    }

    public void setFileType(FileType fileType) {
        this.fileType = fileType;
    }


}
