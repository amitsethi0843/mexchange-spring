package com.mexchange.core.domain.exchange;


import com.mexchange.core.domain.BaseEntity;
import com.mexchange.core.domain.User;
import com.mexchange.core.domain.item.Item;
import com.mexchange.core.utils.AppUtil;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.util.ArrayList;
import java.util.List;

@Entity
@Table(name = "SELLER_EXCHANGE_INSTANCE")
public class SellerExchangeInstance  extends BaseEntity{

    @NotNull
    @OneToOne
    @JoinColumn(name = "USER_ID")
    private User user;

    @NotNull
    @OneToOne
    @JoinColumn(name = "ITEM_ID")
    private Item item;

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public Item getItem() {
        return item;
    }

    public void setItem(Item item) {
        this.item = item;
    }

    public SellerExchangeInstance(){}

    public SellerExchangeInstance(Item item,User user){
        this.user=user;
        this.item=item;
        this.setUuid(AppUtil.getUniqueID());
    }
}
