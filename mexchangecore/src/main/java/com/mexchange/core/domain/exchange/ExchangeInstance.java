package com.mexchange.core.domain.exchange;


import com.mexchange.core.domain.BaseEntity;
import com.mexchange.core.domain.User;
import com.mexchange.core.domain.address.Address;
import com.mexchange.core.domain.contact.Contact;
import com.mexchange.core.utils.AppUtil;
import com.mexchange.core.utils.co.exchange.CoreCreateExchangeCO;

import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

@Entity
@Table(name = "EXCHANGE_INSTANCE")
public class ExchangeInstance extends BaseEntity {

    @OneToOne
    @JoinColumn(name="BUYER_EXCHANGE_ID")
    private BuyerExchangeInstance buyerExchangeInstance;

    @OneToOne
    @JoinColumn(name="SELLER_EXCHANGE_ID")
    private SellerExchangeInstance sellerExchangeInstance;

    @NotNull
    @OneToOne
    @JoinColumn(name = "ADDRESS_ID")
    private Address address;

    @NotNull
    @OneToOne
    @JoinColumn(name = "CONTACT_ID")
    private Contact contact;

    @NotNull
    @OneToOne
    @JoinColumn(name = "USER_ID")
    private User createdBy;

    public Address getAddress() {
        return address;
    }

    public void setAddress(Address address) {
        this.address = address;
    }

    public Contact getContact() {
        return contact;
    }

    public void setContact(Contact contact) {
        this.contact = contact;
    }

    public User getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(User createdBy) {
        this.createdBy = createdBy;
    }

    public BuyerExchangeInstance getBuyerExchangeInstance() {
        return buyerExchangeInstance;
    }

    public void setBuyerExchangeInstance(BuyerExchangeInstance buyerExchangeInstance) {
        this.buyerExchangeInstance = buyerExchangeInstance;
    }

    public SellerExchangeInstance getSellerExchangeInstance() {
        return sellerExchangeInstance;
    }

    public void setSellerExchangeInstance(SellerExchangeInstance sellerExchangeInstance) {
        this.sellerExchangeInstance = sellerExchangeInstance;
    }

    public ExchangeInstance(){}

    public ExchangeInstance(CoreCreateExchangeCO coreCreateExchangeCO){
        this.createdBy=coreCreateExchangeCO.getSellerExchangeInstance().getUser();
        this.address=coreCreateExchangeCO.getAddress();
        this.contact=coreCreateExchangeCO.getContact();
        this.sellerExchangeInstance=coreCreateExchangeCO.getSellerExchangeInstance();
        this.setUuid(AppUtil.getUniqueID());
    }
}
