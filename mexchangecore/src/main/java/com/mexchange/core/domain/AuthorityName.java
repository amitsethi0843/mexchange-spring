package com.mexchange.core.domain;

public enum AuthorityName {
    ROLE_USER, ROLE_ADMIN
}