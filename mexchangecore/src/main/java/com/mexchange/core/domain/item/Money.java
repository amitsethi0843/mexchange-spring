package com.mexchange.core.domain.item;

import com.mexchange.core.domain.BaseEntity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;


@Entity
@Table(name = "MONEY")
public class Money extends BaseEntity {

    @NotNull
    @Column(name = "AMOUNT")
    private Long amount;

}
