package com.mexchange.core.domain;

import com.mexchange.core.utils.AppUtil;

import javax.persistence.*;
import javax.validation.constraints.NotNull;

@Entity
@Table(name = "AUTHORITY")
public class Authority extends BaseEntity {


    @Column(name = "NAME", length = 50)
    @NotNull
    @Enumerated(EnumType.STRING)
    private AuthorityName name;


    public AuthorityName getName() {
        return name;
    }

    public void setName(AuthorityName name) {
        this.name = name;
    }

    public Authority(){}

    public Authority(AuthorityName authorityName){
        this.name=authorityName;
        this.setUuid(AppUtil.getUniqueID());
    }

}