package com.mexchange.core.domain.exchange;



import com.mexchange.core.domain.BaseEntity;
import com.mexchange.core.domain.User;
import com.mexchange.core.domain.item.Item;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.util.ArrayList;
import java.util.List;


@Entity
@Table(name = "BUYER_EXCHANGE_INSTANCE")
public class BuyerExchangeInstance extends BaseEntity{

    @NotNull
    @OneToOne
    @JoinColumn(name = "USER_ID")
    private User user;

    @OneToMany
    @JoinColumn(name = "BUYER_EXCHANGE_INSTANCE_ID")
    private List<Item> itemList=new ArrayList<>();

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public List<Item> getItemList() {
        return itemList;
    }


    public void setItemList(List<Item> itemList) {
        this.itemList.addAll(itemList);
    }

    public BuyerExchangeInstance(){}

    public BuyerExchangeInstance(List<Item> itemList,User user){
        this.setItemList(itemList);
        this.user=user;
    }
}
