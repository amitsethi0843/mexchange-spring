package com.mexchange.core.domain.item;


import com.mexchange.core.domain.BaseEntity;
import com.mexchange.core.utils.AppUtil;
import com.mexchange.core.utils.co.item.CoreCreateItemCO;
import com.mexchange.core.utils.enums.ItemType;
import com.mexchange.core.domain.files.SupportingImage;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

@Entity
@Table(name = "ITEM")
public class Item extends BaseEntity {

    @NotNull
    @Column(name = "NAME", length = 50)
    private String name;

    @Column(name="QUANTITY")
    private Long quantity=1L;

    @Column(name = "TYPE")
    @Enumerated(EnumType.STRING)
    private ItemType itemType;

    @OneToMany
    @JoinColumn(name="ITEM_ID")
    private List<SupportingImage> images=new ArrayList<SupportingImage>();

    @Lob
    private String itemDescription;

    public List<SupportingImage> getImages() {
        return images;
    }

    public void setImages(List<SupportingImage> images) {
        this.images = images;
    }

    public String getItemDescription() {
        return itemDescription;
    }

    public void setItemDescription(String itemDescription) {
        this.itemDescription = itemDescription;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Long getQuantity() {
        return quantity;
    }

    public void setQuantity(Long quantity) {
        this.quantity = quantity;
    }

    public ItemType getItemType() {
        return itemType;
    }

    public void setItemType(ItemType itemType) {
        this.itemType = itemType;
    }



    public Item(){}

    public Item(CoreCreateItemCO coreCreateItemCO){
        this.setUuid(AppUtil.getUniqueID());
        this.name=coreCreateItemCO.getName();
        this.quantity=coreCreateItemCO.getQuantity();
        this.itemType=coreCreateItemCO.getItemType();
        this.itemDescription=coreCreateItemCO.getItemDescription();
    }
}
