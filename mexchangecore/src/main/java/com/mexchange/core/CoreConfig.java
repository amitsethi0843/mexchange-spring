package com.mexchange.core;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.context.MessageSourceAutoConfiguration;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.boot.autoconfigure.liquibase.LiquibaseAutoConfiguration;
import org.springframework.context.MessageSource;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.PropertySource;
import org.springframework.context.support.ReloadableResourceBundleMessageSource;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

@SpringBootApplication(exclude = {LiquibaseAutoConfiguration.class,MessageSourceAutoConfiguration.class})
@EntityScan(basePackages = {"com.mexchange.core.domain"})
@EnableJpaRepositories(basePackages = {"com.mexchange.core.repository"})
@ComponentScan(basePackages = {"com.mexchange.core","org.springframework.security.crypto"})
//@PropertySource({"file:${propertiesPath}/config/rest/application-${env}.properties",
//	"file:${propertiesPath}/config/core/application-${env}.properties"})
public class CoreConfig {

//    @Bean
//    public MessageSource messageSource() {
//        ReloadableResourceBundleMessageSource messageSource = new ReloadableResourceBundleMessageSource();
//        messageSource.setBasename("classpath:messages");
//        messageSource.setCacheSeconds(10); //reload messages every 10 seconds
//        return messageSource;
//    }

    public static void main(String[] args) {
        SpringApplication.run(CoreConfig.class, args);
    }

}
